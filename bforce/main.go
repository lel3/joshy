package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"
)

type workerPool struct {
	maxWorker   int
	queuedTaskC chan func()
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("specify a key to search for")
		return
	}
	// generate the charset
	charSet := getFullCharSet()

	poolSize := runtime.NumCPU()
	target := os.Args[1]
	encodedTarget := getEncodedString(target, charSet)
	var candidates uint64 = 0
	for i := 1; i <= len(target); i++ {
		candidates += uint64(math.Pow(float64(len(charSet)), float64(i)))
	}
	candidates--

	fmt.Printf("working on %v char: \"%v\"\n", len(charSet), string(charSet))
	fmt.Println("target:", target)
	fmt.Println("encoded target:", encodedTarget)
	fmt.Printf("max iterations: %v\n", int(candidates))
	fmt.Println("pool size:", poolSize)

	wp := workerPool{poolSize, make(chan func(), poolSize)}
	wp.Run()

	progressC := make(chan uint64, poolSize)
	progressMutex := sync.Mutex{}
	progressTicker := time.NewTicker(5 * time.Second)
	var startTime time.Time
	go func() {
		var processed uint64 = 0
		go func() {
			for range progressTicker.C {
				log.Printf("processed %v values: %.2f%%\n", processed, float64(processed)/float64(candidates)*100)
			}
		}()
		for p := range progressC {
			progressMutex.Lock()
			processed += p
			progressMutex.Unlock()
		}
	}()

	resultC := make(chan uint64)
	go func() {
		select {
		case r := <-resultC:
			log.Printf("got encoded result in %v: %v", time.Since(startTime), r)
			os.Exit(0)
		}
	}()

	step := uint64(math.Ceil(float64(candidates) / float64(poolSize)))
	startTime = time.Now()
	for i := 0; i < poolSize; i++ {
		gg := i
		wp.AddTask(func() {
			offset := uint64(gg) * step
			worker(offset, offset+step, encodedTarget, resultC, progressC)
		})
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGKILL)
	<-c
	fmt.Println("shutting down...")
}

func (wp *workerPool) Run() {
	for i := 0; i < wp.maxWorker; i++ {
		go func(workerID int) {
			for task := range wp.queuedTaskC {
				task()
			}
		}(i + 1)
	}
}

func (wp *workerPool) AddTask(task func()) {
	wp.queuedTaskC <- task
}

func worker(from, to uint64, wanted uint64, result, progress chan uint64) {
	//fmt.Printf("new worker from %v to %v, looking for %v\n", from, to, wanted)
	t := time.NewTicker(4900 * time.Millisecond)
	i := from
	go func() {
		lastCheck := from
		for range t.C {
			progress <- i - lastCheck
			lastCheck = i
		}
	}()
	for ; i < to; i++ {
		if i == wanted {
			result <- i
			t.Stop()
			return
		}
	}
}

// given a string, it returns the solution number
// charSet: a-zA-Z0-9
// a => 0
// aa => 62
// ab => 63
func getEncodedString(plain string, charSet []rune) uint64 {
	idxInSet := func(c rune) int {
		for i, r := range charSet {
			if c == r {
				return i
			}
		}
		return -1
	}
	baseLength := float64(len(charSet))

	var result uint64 = 0
	for i := 0; i < len(plain); i++ {
		result += uint64(math.Pow(baseLength, float64(i)))
	}
	result--

	for i, c := range plain {
		pos := len(plain) - 1 - i
		idx := float64(idxInSet(c))

		result += uint64(idx * math.Pow(baseLength, float64(pos)))
	}
	return result
}

func getFullCharSet() []rune {
	charSet := make([]rune, 62, 68)
	i := 0
	for c := 'a'; c <= 'z'; c++ {
		charSet[i] = c
		i++
	}
	for c := 'A'; c <= 'Z'; c++ {
		charSet[i] = c
		i++
	}
	for c := '0'; c <= '9'; c++ {
		charSet[i] = c
		i++
	}
	charSet = append(charSet, []rune{'(', ')', '.', ',', '-', '!'}...)
	return charSet
}
