package main

import "testing"

func Test_getEncodedString(t *testing.T) {
	type args struct {
		plain   string
		charSet []rune
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			"first single char",
			args{"a", getFullCharSet()},
			0,
		},
		{
			"second single char",
			args{"b", getFullCharSet()},
			1,
		},
		{
			"third single char",
			args{"c", getFullCharSet()},
			2,
		},
		{
			"first double char",
			args{"aa", getFullCharSet()},
			62,
		},
		{
			"second double char",
			args{"ab", getFullCharSet()},
			63,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getEncodedString(tt.args.plain, tt.args.charSet); got != tt.want {
				t.Errorf("getEncodedString() = %v, want %v", got, tt.want)
			}
		})
	}
}
