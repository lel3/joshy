(working with all lowercase)

##key
- if the key contains a J, replace it wit I => the key must not contain J
- table filled top to bottom, left to right

##encryption
- standard rules
  - both letters on the same line:
  - take the letter to the right (cyclical table)
  - both letters on the same column:
  - take the letter below (cyclical table)
    - letters forming a rectangle:
  - take the letter on the opposite corner

- my custom rules
  - if a pair is composed of identical letters:
      - if pair != "xx" => split with x ("abccd" => "abcxcd")
      - else split with z ("abxxd" => "abxzxd")
  - if a letter is standing alone:
      - letter += letter == "x" ? "z" : "x"
  - if a letter (both in pair or alone) == "j"
      - letter = i

##note
here the only flaw I can immediately spot (other than the obvious ones, like numbers, spaces etc) is that when
decoding the cyphered message it's impossible to establish if the original letter is I or J, given the encoded I.




