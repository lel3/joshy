package main

import (
	"fmt"
	"os"
	"strings"
)

// table containing the key values, hash as index
var keyTable [5][5]rune
var idxKey map[rune]byte

func main() {
	if l := len(os.Args); l != 3 {
		fmt.Println("expecting 2 arguments, got", l-1)
		fmt.Printf("%v [key] [plain text]", os.Args[0])
		return
	}
	setKeyTable(os.Args[1])
	encrypted := encrypt(os.Args[2])
	fmt.Printf("\n%s: %s\n", os.Args[2], encrypted)
	fmt.Printf("%s: %s\n", encrypted, decrypt(encrypted))
}

func setKeyTable(key string) {
	// first normalize the key
	key = strings.ToLower(strings.ReplaceAll(key, " ", ""))

	// clear values, hash as index
	keyTable = [5][5]rune{}
	idxKey = make(map[rune]byte, 25)

	fill := func(position *byte, letter rune) {
		if letter == 'j' {
			return
		}
		// insert the letter only if it's unique
		if _, ok := idxKey[letter]; !ok {
			// store the position in the index
			idxKey[letter] = *position
			// put the letter in the table
			keyTable[*position/5][*position%5] = letter
			*position++
		}
	}

	// put the key in the table
	var nLetters byte = 0
	for _, l := range key {
		if nLetters == 25 {
			return
		}
		fill(&nLetters, l)
	}

	// letters: [97, 122]
	for i := 'a'; i <= 'z' && nLetters < 25; i++ {
		fill(&nLetters, i)
	}
}

func encrypt(message string) string {
	return scrambler(message, 6)
}
func decrypt(message string) string {
	return scrambler(message, 4)
}

func scrambler(message string, step byte) string {
	if step == 6 {
		// normalize the text first, we're encoding
		message = strings.NewReplacer(" ", "", "j", "i").Replace(strings.ToLower(message))
	}
	text := []rune(message)

	// traverse the text and encrypt every pair
	for i := 0; i < len(text); i += 2 {
		// in case we're at the end of an odd text, append THE spare letter
		// safe, never happen with decryption
		if i+1 == len(text) {
			if text[i] == 'x' {
				text = append(text, 'z')
			} else {
				text = append(text, 'x')
			}
		}
		left := text[i]
		right := text[i+1]
		if step == 6 {
			// two identical letters. split them, only when encoding
			if left == right && left == 'x' {
				text = append(text[:i+1], append([]rune{'z'}, text[i+1:]...)...)
				right = 'z'
			} else if left == right {
				text = append(text[:i+1], append([]rune{'x'}, text[i+1:]...)...)
				right = 'x'
			}
		}
		posLeft := idxKey[left]
		posRight := idxKey[right]

		if pLine := posLeft / 5; pLine == posRight/5 {
			// same line
			left = keyTable[pLine][(posLeft+step)%5]
			right = keyTable[pLine][(posRight+step)%5]
		} else if pCol := posLeft % 5; pCol == posRight%5 {
			// same column
			left = keyTable[(posLeft/5+step)%5][pCol]
			right = keyTable[(posRight/5+step)%5][pCol]
		} else {
			// opposite corners: same line as the original, column of the other letter
			left = keyTable[posLeft/5][posRight%5]
			right = keyTable[posRight/5][posLeft%5]
		}
		text[i] = left
		text[i+1] = right
	}
	return string(text)
}
