package main

import (
	"reflect"
	"testing"
)

func Test_getKeyTable(t *testing.T) {
	type args struct {
		key string
	}
	tests := []struct {
		name      string
		args      args
		wantTable [5][5]rune
		wantIndex map[rune]byte
	}{
		{
			"simple test 1",
			args{
				"My Qey hey",
			},
			[5][5]rune{
				{'m', 'y', 'q', 'e', 'h'},
				{'a', 'b', 'c', 'd', 'f'},
				{'g', 'i', 'k', 'l', 'n'},
				{'o', 'p', 'r', 's', 't'},
				{'u', 'v', 'w', 'x', 'z'},
			},
			map[rune]byte{
				'm': 0, 'y': 1, 'q': 2, 'e': 3, 'h': 4,
				'a': 5, 'b': 6, 'c': 7, 'd': 8, 'f': 9,
				'g': 10, 'i': 11, 'k': 12, 'l': 13, 'n': 14,
				'o': 15, 'p': 16, 'r': 17, 's': 18, 't': 19,
				'u': 20, 'v': 21, 'w': 22, 'x': 23, 'z': 24,
			},
		},
		{
			"full alphabet",
			args{
				"abcdefghijklmnopqrstuvwxyz",
			},
			[5][5]rune{
				{'a', 'b', 'c', 'd', 'e'},
				{'f', 'g', 'h', 'i', 'k'},
				{'l', 'm', 'n', 'o', 'p'},
				{'q', 'r', 's', 't', 'u'},
				{'v', 'w', 'x', 'y', 'z'},
			},
			map[rune]byte{
				'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4,
				'f': 5, 'g': 6, 'h': 7, 'i': 8, 'k': 9,
				'l': 10, 'm': 11, 'n': 12, 'o': 13, 'p': 14,
				'q': 15, 'r': 16, 's': 17, 't': 18, 'u': 19,
				'v': 20, 'w': 21, 'x': 22, 'y': 23, 'z': 24,
			},
		},
		{
			"long key",
			args{
				"abcdefghijklmnopqrstuvwxy very very long key z",
			},
			[5][5]rune{
				{'a', 'b', 'c', 'd', 'e'},
				{'f', 'g', 'h', 'i', 'k'},
				{'l', 'm', 'n', 'o', 'p'},
				{'q', 'r', 's', 't', 'u'},
				{'v', 'w', 'x', 'y', 'z'},
			},
			map[rune]byte{
				'a': 0, 'b': 1, 'c': 2, 'd': 3, 'e': 4,
				'f': 5, 'g': 6, 'h': 7, 'i': 8, 'k': 9,
				'l': 10, 'm': 11, 'n': 12, 'o': 13, 'p': 14,
				'q': 15, 'r': 16, 's': 17, 't': 18, 'u': 19,
				'v': 20, 'w': 21, 'x': 22, 'y': 23, 'z': 24,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			setKeyTable(tt.args.key)
			if !reflect.DeepEqual(keyTable, tt.wantTable) {
				t.Errorf("getKeyTable() got table = \n%v, want \n%v", keyTable, tt.wantTable)
			}
			if !reflect.DeepEqual(idxKey, tt.wantIndex) {
				t.Errorf("getKeyTable() got index = \n%v, want \n%v", idxKey, tt.wantIndex)
			}
		})
	}
}

func Test_encrypt(t *testing.T) {
	// set a well known key
	setKeyTable("abcdefghijklmnopqrstuvwxyz")

	tests := []struct {
		name string
		args string
		want string
	}{
		{
			"same line 1",
			"ab",
			"bc",
		},
		{
			"same line 2 - overflow",
			"yz",
			"zv",
		},
		{
			"same column 1",
			"af",
			"fl",
		},
		{
			"same column 2 - overflow",
			"uz",
			"ze",
		},
		{
			"rectangle",
			"an",
			"cl",
		},
		{
			"containing J",
			"ej",
			"dk",
		},
		{
			"double letter",
			"abccd", // becomes abcxcd
			"bchcde",
		},
		{
			"double letter X",
			"abxxd", // becomes abxzxd
			"bcyvyc",
		},
		{
			"last letter X",
			"abx", // becomes abxz
			"bcyv",
		},
		{
			"last letter != X",
			"abc", // becomes abcx
			"bchc",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := encrypt(tt.args); got != tt.want {
				t.Errorf("encrypt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_scrambler(t *testing.T) {
	setKeyTable("abcdefghijklmnopqrstuvwxyz")
	type args struct {
		message string
		step    byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"encrypt ab",
			args{
				"ab",
				6,
			},
			"bc",
		},
		{
			"decrypt bc",
			args{
				"bc",
				4,
			},
			"ab",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := scrambler(tt.args.message, tt.args.step); got != tt.want {
				t.Errorf("scrambler() = %v, want %v", got, tt.want)
			}
		})
	}
}
